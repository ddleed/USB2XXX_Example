package com.usbxyz.usb2xxxdemo.USB2LIN;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.toomoss.USB2XXX.USB2LIN_EX;
import com.toomoss.USB2XXX.USB_Device;
import com.usbxyz.usb2xxxdemo.R;
import com.usbxyz.usb2xxxdemo.USBManagerActivity;

import java.util.HashMap;

public class LINMasterActivity extends AppCompatActivity {
    private static final String ACTION_USB_PERMISSION ="com.usbxyz.USB_PERMISSION";
    //导入库，必须要
    static{
        System.loadLibrary("jnidispatch");
        System.loadLibrary("USB2XXX");
        System.loadLibrary("usb");
    }
    USBManagerActivity mUSBManagerActivity = new USBManagerActivity();
    private PendingIntent pendingIntent;
    TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_linmaster);
        //初始化USB
        mUSBManagerActivity.USBInit(this);
        textView = findViewById(R.id.textView);
        textView.setMovementMethod(ScrollingMovementMethod.getInstance());
        Button button = findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textView.setText(null);
                int ret;
                int DevHandle = 0;
                boolean state;
                int[] DevHandleArry = new int[20];
                //扫描设备
                ret = USB_Device.INSTANCE.USB_ScanDevice(DevHandleArry);
                if(ret > 0){
                    textView.append("DeviceNum = "+ret+"\n");
                    for(int i=0;i<ret;i++) {
                        textView.append("DevHandle = " + String.format("0x%08X", DevHandleArry[i]) + "\n");
                    }
                }else{
                    textView.append("No device "+String.format("%d\n",ret));
                    return;
                }
                DevHandle = DevHandleArry[0];
                //打开设备
                state = USB_Device.INSTANCE.USB_OpenDevice(DevHandle);
                if(!state){
                    textView.append("Open device error");
                    return;
                }else{
                    textView.append("Open device success\n");
                }
                //获取设备信息
                try {
                    USB_Device.DEVICE_INFO DevInfo = new USB_Device.DEVICE_INFO();
                    byte[] funcStr = new byte[128];
                    state = USB_Device.INSTANCE.DEV_GetDeviceInfo(DevHandle,DevInfo,funcStr);
                    if(!state){
                        USB_Device.INSTANCE.USB_CloseDevice(DevHandle);
                        textView.append("Get device infomation error");
                        return;
                    }else{
                        textView.append("Firmware Info:\n");
                        textView.append("--Name:" + new String(DevInfo.FirmwareName, "UTF-8")+"\n");
                        textView.append("--Build Date:" + new String(DevInfo.BuildDate, "UTF-8")+"\n");
                        textView.append(String.format("--Firmware Version:v%d.%d.%d\n", (DevInfo.FirmwareVersion >> 24) & 0xFF, (DevInfo.FirmwareVersion >> 16) & 0xFF, DevInfo.FirmwareVersion & 0xFFFF));
                        textView.append(String.format("--Hardware Version:v%d.%d.%d\n", (DevInfo.HardwareVersion >> 24) & 0xFF, (DevInfo.HardwareVersion >> 16) & 0xFF, DevInfo.HardwareVersion & 0xFFFF));
                        textView.append("--Functions:" + new String(funcStr, "UTF-8")+"\n");
                    }
                } catch (Exception ep) {
                    ep.printStackTrace();
                }
                //初始化设备
                ret = USB2LIN_EX.INSTANCE.LIN_EX_Init(DevHandle,(byte)0,19200,(byte)1);
                if(ret == USB2LIN_EX.LIN_EX_SUCCESS){
                    textView.append("Init device success\n");
                }else{
                    textView.append("Init device error ret = "+String.format("%d",ret));
                }
                //主机模式发送数据
                byte write_pid = 0x01;
                byte[] write_data={0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08};
                ret = USB2LIN_EX.INSTANCE.LIN_EX_MasterWrite(DevHandle,(byte)0,write_pid,write_data,(byte)write_data.length,USB2LIN_EX.LIN_EX_CHECK_EXT);
                if(ret == USB2LIN_EX.LIN_EX_SUCCESS){
                    textView.append("Write data success\n");
                }else{
                    textView.append("Write data ret = "+String.format("%d",ret));
                }
                //主机模式读取数据
                byte read_pid = 0x02;
                byte[] read_data=new byte[8];
                ret = USB2LIN_EX.INSTANCE.LIN_EX_MasterRead(DevHandle,(byte)0,read_pid,read_data);
                if(ret > 0){
                    textView.append("Read Data:\n");
                    for(int i=0;i<ret;i++){
                        textView.append(String.format("0x%02X ", read_data[i]));
                    }
                    textView.append("\n");
                }else if(ret ==0){
                    textView.append("slave not responding");
                }else{
                    textView.append("Read data error ret = "+String.format("%d",ret));
                }
                //关闭设备
                //USB_Device.INSTANCE.USB_CloseDevice(DevHandle);
            }
        });
    }
}
